var net = require('net');
var ClientsList = require('./ClientsList.js');

//////////////////
var PORT = 43234;
var ClientsList;

main();

function main() {

	ClientsList = new ClientsList();

	initServer(ClientsList);

	var consoleInput = function() {
		input( ">> ", function(data) {
			var parts = data.trim().split(' ');
			if (parts[0]) parts[0] = parts[0].toUpperCase();

			switch (true) {
				case (parts[0]=="Q"):
					process.exit(0);
					break;
				case (parts[0]=="LIST" && parts[1]=="CHANNELS"):
					let channels = ClientsList.getChannels();
					var subscribers = [];
					var stats = [];
					console.log("List of channels: \n");
					for(let i = 0; i < channels.length; i++){
						console.log(channels[i] + "\n");
						//Recover clients
						subscribers = ClientsList.getSubscribers(channels[i]);
						for(let j = 0; j < subscribers.length; j++){
							console.log(" - " + subscribers[j] + "\n");
							stats = ClientsList.getSubscriberDetail(subscribers[j]);
							console.log("  id_socket: " + stats[0].id_socket + "\n\n");
						}
					}
					consoleInput();
					break;

				case (parts[0]=="LIST" && parts[1]=="CHANNEL"):
					//Check if exists parts[2]
					if(parts[2] == null){
						console.log("You need to especify the name of the channel.");
						consoleInput();
						break;
					}
					
					//Check if the channel exits
					var channelExists = false;
					var allChannels = ClientsList.getChannels();
					for(let i = 0; i < allChannels.length; i++){
						if(allChannels[i] == parts[2]){
							channelExists = true;
						}
					}
					if(!channelExists){
						console.log("This channel does not exists");
						consoleInput();
						break;
					}

					subscribers = ClientsList.getSubscribers(parts[2]);
					console.log("Subscribers in channel " + parts[2] + ":\n");
					for(let i = 0; i < subscribers.length; i++){
						console.log(" - " + subscribers[i] + "\n");
					}
					consoleInput();
					break;	

				case (parts[0]=="SENDALL"):
					//Check if the cleint especify the channel to send and the message
					if(parts.length < 3){
						console.log("You need to specify the channel and the message to send");
					}

					//Check if the channel exits
					var channelExists = false;
					var allChannels = ClientsList.getChannels();
					for(let i = 0; i < allChannels.length; i++){
						if(allChannels[i] == parts[1]){
							channelExists = true;
						}
					}
					if(!channelExists){
						console.log("This channel does not exists");
						consoleInput();
						break;
					}
					//Recover the message to send
					var message = "";
					for(let i = 2; i <  parts.length; i++){
						message += parts[i] + " ";
					}
					//Recover the sockets of the channel
					//clientsList.getSubscriberSockets(subscribers[i])[0]
					console.log(parts[1]);
					var sockets = ClientsList.getSockets(parts[1]);
					
					for(let i = 0; i < sockets.length; i++){
						sockets[i].write(message, ()=>{});
					}
					
					consoleInput();
					break;

				case (parts[0]=="SEND"):
					//Hint: use ClientsList.getSubscribersDetail() and ClientsList.send();
					//Check if the cleint especify the channel to send and the message
					if(parts.length < 3){
						console.log("You need to specify the subscriber and the message to send");
					}

					//Check if the subsciber exits
					var subscriberExists = false;
					var allChannels = ClientsList.getChannels();
					var subscriber;
					for(let i = 0; i < allChannels.length; i++){
						subscriber = ClientsList.getSubscribers(allChannels[i]);
						for(let j = 0; j < subscriber.length; j++){
							if(subscriber[j] == parts[1]){
								subscriberExists = true;
							}
						}
						
					}

					if(!subscriberExists){
						console.log("This subscriber does not exists");
						consoleInput();
						break;
					}

					//Recover the message to send
					var message = "";
					for(let i = 2; i <  parts.length; i++){
						message += parts[i] + " ";
					}
					//Recover the sockets of the channel
					var sockets = ClientsList.getSubscriberSockets(parts[1]);
					
					sockets[0].write(message, ()=>{});
									
					consoleInput();
					break;
					
				case (parts[0]=="WHEREIS"):
					if (parts[1]) {
						var details = ClientsList.getSubscriberDetail(parts[1]);
						console.log ( JSON.stringify(  details ) );
					}
					setTimeout( consoleInput, 300);
					break;
				default:
					console.log("Incorrect command");
					consoleInput();
					break;
			}

		});
	};

	console.log("----------------------------")
	console.log("LIST CHANNELS - List registered CHANNELS and its sockets and stats");
	console.log("LIST CHANNEL [CHANNEL] - List regsitered subscribers in the CHANNEL");
	console.log("WHEREIS [SUBSCRIBER] - List CHANNEL and sockets of subscriber SUBSCRIBER");
	console.log("SEND [SUBSCRIBER] [MESSAGE] - Send MESSAGE to the SUBSCRIBER client identified")
	console.log("SENDALL [CHANNEL] [MESSAGE] - Send MESSAGE to all subscribers in the CHANNEL")
	console.log("----------------------------")
	console.log("Q - Exit");
	console.log("----------------------------")

	consoleInput();
}


function initServer(clientsList) {
	net.createServer(function(sock) {
		sock.id = Math.floor(Math.random() * 100000);
			console.log('CONNECTION RECEIVED: ' + sock.remoteAddress +':'+ sock.remotePort);
	    sock.on('data', function(data) {
	    	var _self = this;
			console.log('DATA ' + sock.remoteAddress + ': ' + data);
			data.toString('utf-8').split('\n').forEach(function(message){
	    		parseData(clientsList, message, _self);
	    	});
	    });
	    sock.on('close', function(data) {
	    	console.log('Connection closed for socket ' + sock.id);
	    	clientsList.removeSocket(sock.id);
	    });
	    sock.on('error', function (err) {
		    console.log("Socket error " + sock.id, err.stack);
		    clientsList.removeSocket(sock.id);
		});
	    
	}).listen(PORT);
	console.log('Server listening on port '+ PORT);
}

function parseData(clientsList, message, clientSocked) {
    console.log("Parsing message: " + message + "\n");
    //Pasamos mensaje a todos
	var messageParts = message.trim().split(' ');
	
    switch(true){
		case (messageParts[0] == "REG"):
			clientsList.setChannelToClient(clientSocked.id, messageParts[2], messageParts[1], clientSocked);
			break;

		case (messageParts[0] == "MSG"):
			//Recover all the clients and check if messageParts[1] there aren't the name of the user in the channel
			var channel = clientsList.getChannel(clientSocked.id);
			var objectiveUser = "";
			var missageToSend = "";
			var subscribers = clientsList.getSubscribers(channel);
			for(let i = 0; i < subscribers.length; i++){
				if(subscribers[i] == messageParts[1]){
					objectiveUser = subscribers[i];
				}
			}

			//Recover message to send without MSG
			for(let i = 1; i < messageParts.length; i++){
				if(objectiveUser != "" && i == 1){
					missageToSend = "Missage of " + clientsList.getSubscriber(clientSocked.id) + ": ";
				}
				else{
					missageToSend += messageParts[i] + " ";
				}
			}

			//MSG message
			//Recover the sockets of the subscribers
			if(objectiveUser == ""){
				for(let i = 0; i < subscribers.length; i++){
					if(clientsList.getSubscriberSockets(subscribers[i])[0].id != clientSocked.id){
						clientsList.getSubscriberSockets(subscribers[i])[0].write(missageToSend, ()=>{});
					}
				}
			}

			//MSG user message
			else{
				clientsList.getSubscriberSockets(objectiveUser)[0].write(missageToSend, ()=>{});
			}
			
						
			break;
            
    }

	//Si la primera posicion del message es un REG tenim que fer un setChannelToClient per afeigir un nou client amb els perametres.
	//Tambe controlar altres parametres del client
	// To register users:
	// clientsList.setChannelToClient(sock.id, id_player, id_channel);
	// Example: var parts = message.trim().split(':');
	// var game = part[1];
	// var player = parts[2];
	// clientsList.setChannelToClient(clientSocked.id, player, game);
}


function input(question, callback) {
	var stdin = process.stdin, stdout = process.stdout;

	stdin.resume();
	stdout.write(question);

	stdin.once('data', function(data) {
	   data = data.toString().trim();
	   callback(data);
	});
}