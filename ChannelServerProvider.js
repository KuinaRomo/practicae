function ChannelServerProvider() {
	var SERVERPORT = 43234;

	this.channelServer = null;
	this.onConnectionClosed = null;

	this.registerToChannel = function(server, channel, subscriber, callback) {

		var _self = this;
		_self.newSocketToChannelServer(server, function(socket) {
			var fnDone = function() {				 
				callback();
			};
			if (socket) {
				socket.on('close', function() {
					socket.destroy();
					if (typeof this.onConnectionClose == "function") {
						this.onConnectionClosed(_self.channelServer[this.id].server);
					}
					delete _self.channelServer;
					console.log('Connection closed');
				});

				socket.on('error', function() {
					console.log('Connection error');
				});

				socket.on('data', function(data) {
					console.log('Server said:' + data.toString('utf-8'));
				});

				console.log("Registering '" + subscriber + "' to channel '" + channel +  "' in server " + server);
			    socket.write("REG " + channel + " " + subscriber + "\n", function() {
			    	fnDone();			    	
			    });

				_self.channelServer = { server: server, socket: socket };				
			} else {
				fnDone();
			}
		});
	};

	this.getChannelServerSocket = function(server) {
		var socket;
		if (this.channelServer) {
			socket = this.channelServer.socket;			
		}
		return socket;
	};

	this.newSocketToChannelServer = function(server, callback) {
		console.log("Connecting to " + server + " port " + SERVERPORT );
		var net = require('net') , socket = new net.Socket();
		socket.id = Math.floor(Math.random() * 100000);
		socket.setNoDelay(true);
		socket.connect(SERVERPORT , server, function() {
			console.log("Connected to " + server );
			callback(socket);
		});
		socket.on('error', function(e) {
			console.log("Error connecting to " + server );
			callback(null);
	    });
	};
}


module.exports = ChannelServerProvider;
